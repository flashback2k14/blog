class Post < ActiveRecord::Base
	# Posts pointed to Comments
	# ein Post kann mehrere Kommentare haben
	# wenn ein Post geloescht wird, 
	# dann sollen auch die Kommentare geloescht werden 
	has_many :comments, dependent: :destroy
	# Validierung
	# Title muss vorhanden sein
	# Body muss vorhanden sein
	validates_presence_of :title
	validates_presence_of :body

	def self.search(query)
		where("title like ?", "%#{query}%")
	end
end