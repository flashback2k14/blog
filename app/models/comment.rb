class Comment < ActiveRecord::Base
	# Comments pointed to one Post
	# Kommentare koennen nur an einen Post gerichtet sein
	belongs_to :post
	# Validierung
	# Post Id muss vorhanden sein
	# Body muss vorhanden sein
	validates_presence_of :post_id
	validates_presence_of :body
end